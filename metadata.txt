[general]
name=Locate Plugin
description=This plugin permit to translat sexagecimal coordonate into decimal and locate the point in the canvas.
version=1.0
qgisMinimumVersion=3.0
author=Guillaume Delplanque
email=delpro.guillaume@gmail.com