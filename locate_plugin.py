import os
import sys
import inspect
import math
from qgis.core import QgsRectangle, QgsPoint, QgsCoordinateReferenceSystem, QgsCoordinateTransform, QgsProject, QgsGeometry
from PyQt5.QtWidgets import QAction
from PyQt5.QtGui import QIcon
from .locate_plugin_dialog import LocatePluginDialog

cmd_folder = os.path.split(inspect.getfile(inspect.currentframe()))[0]

class LocatePlugin:
    def __init__(self, iface):
        self.iface = iface

    def initGui(self):
        icon = os.path.join(os.path.join(cmd_folder, 'logo.png'))
        self.action = QAction(QIcon(icon), 'Locate', self.iface.mainWindow())
        self.action.triggered.connect(self.run)
        self.iface.addPluginToMenu('&Locate', self.action)
        self.iface.addToolBarIcon(self.action)
        self.first_start = True

    def unload(self):
        self.iface.removeToolBarIcon(self.action)
        self.iface.removePluginMenu('&Locate', self.action)
        del self.action

    def clear(self):
        """
            The function clear all the components from the GUI
        """
        # self.dialog.le_input_latitude.setText("47°12\'36.0\"N")
        # self.dialog.le_input_longitude.setText("1°44\'38.4\"W")
        self.dialog.le_input_latitude.setText("47.209991")
        self.dialog.le_input_longitude.setText("-1.744007")
        self.dialog.le_output_lati.clear()
        self.dialog.le_output_long.clear()

    def take_epsg_from_project(self):
        epsg_project = self.iface.mapCanvas().mapSettings().destinationCrs().authid()
        return epsg_project

    def set_espg_source(self):
        return QgsCoordinateReferenceSystem(4326)

    def set_transformation(self, source, destination):
        transformation = QgsCoordinateTransform(source, destination, QgsProject.instance())
        return transformation

    def set_epsg_destination(self, epsg_project):
        return QgsCoordinateReferenceSystem(epsg_project)

    def check_if_float(self, value):
        try:
            float(value)
            flag = True
        except:
            flag = False
        return flag

    def set_orientation_latitude(self, latitude):
        orientation = ''

        if float(latitude) >= 0:
            orientation = 'N'
        else:
            orientation = 'S'
        return orientation

    def set_orientation_longitude(self, longitude):
        orientation = ''

        if float(longitude) >= 0:
            orientation = 'E'
        else:
            orientation = 'W'
        return orientation

    def set_unsigned_value(self, value):
        if float(value) < 0:
            unsigned_value = str(float(value) * -1)
        else:
            unsigned_value = value
        return unsigned_value

    def format_sexagecimal(self, sexagecimal_str):

        degrees = 0.0
        minutes = 0.0
        seconds = 0.0
        orientation = ""

        indice_degrees = 0
        for car in sexagecimal_str:
            if car == '°':
                degrees = int(sexagecimal_str[0:indice_degrees])
                break
            indice_degrees += 1

        indice_minutes = 0
        for car in sexagecimal_str:
            if car == "'":
                minutes = int(sexagecimal_str[indice_degrees+1:indice_minutes])
                break
            indice_minutes += 1

        indice_seconds = 0
        for car in sexagecimal_str:

            if indice_seconds < 1:
                pass
            else:
                if car == '\"':
                    seconds = float(sexagecimal_str[indice_minutes+1:indice_seconds])
                    break
            indice_seconds += 1

        indice_orientation = 0
        for car in sexagecimal_str:

            if car in ('N', 'S', 'W', 'E', 'O'):
                orientation = car
                break
            indice_orientation += 1

        return [degrees, minutes, seconds, orientation]

    def convert_decimal_to_sexagecimal(self, decimal, orientation):

        degrees = 0.0
        minutes = 0.0
        seconds = 0.0

        indice_degrees = 0
        for car in decimal:
            if car == '.':
                degrees = int(decimal[0:indice_degrees])
                break
            indice_degrees += 1

        convert_to_minutes = round((float(decimal) - degrees) * 60, 6)

        indice_minutes = 0
        for car in str(convert_to_minutes):

            if car == '.':
                minutes = str(convert_to_minutes)[0:indice_minutes]
                break
            indice_minutes += 1

        seconds = round((float(convert_to_minutes) - float(minutes)) * 60, 6)
        sexagecimal = "{}°{}\'{}\"{}".format(degrees, minutes, seconds, orientation)
        return sexagecimal

    def convert_sexagecimal_to_decimal(self, degrees, minutes, seconds, orientation):
        if orientation in ('W', 'O', 'S'):
            decimal = (degrees * -1) - (minutes / 60) - (seconds / 3600)
        else:
            decimal = degrees + (minutes / 60) + (seconds / 3600)

        return round(decimal, 6)

    def convert(self):
        canvas = self.iface.mapCanvas()
        scale = 0.0003
        epsg_project = self.take_epsg_from_project()
        self.dialog.lbl_espg_project.setText(epsg_project)
        epsg_source = self.set_espg_source()
        epsg_destination = self.set_epsg_destination(epsg_project)
        epsg_transformation = self.set_transformation(
            epsg_source, epsg_destination)

        latitude = self.dialog.le_input_latitude.text()
        longitude = self.dialog.le_input_longitude.text()

        if self.check_if_float(latitude) and self.check_if_float(longitude):

            latitude_value = self.dialog.le_input_latitude.text()
            longitude_value = self.dialog.le_input_longitude.text()
            deci_latitude = float(latitude_value)
            deci_longitude = float(longitude_value)

            orientation_latitude = self.set_orientation_latitude(
                latitude_value)
            orientation_longitude = self.set_orientation_longitude(
                longitude_value)

            unsigned_latitude = self.set_unsigned_value(latitude_value)
            unsigned_longitude = self.set_unsigned_value(longitude_value)

            sexa_latitude = self.convert_decimal_to_sexagecimal(
                unsigned_latitude, orientation_latitude)
            sexa_longitude = self.convert_decimal_to_sexagecimal(
                unsigned_longitude, orientation_longitude)

            self.dialog.le_output_wgs84_lati.setText(sexa_latitude)
            self.dialog.le_output_wgs84_long.setText(sexa_longitude)
        else:
            sexa_latitude = self.format_sexagecimal(
                self.dialog.le_input_latitude.text())
            sexa_longitude = self.format_sexagecimal(
                self.dialog.le_input_longitude.text())

            deci_latitude = self.convert_sexagecimal_to_decimal(
                sexa_latitude[0],
                sexa_latitude[1],
                sexa_latitude[2],
                sexa_latitude[3],
                )
            deci_longitude = self.convert_sexagecimal_to_decimal(
                sexa_longitude[0],
                sexa_longitude[1],
                sexa_longitude[2],
                sexa_longitude[3]
                )

            self.dialog.le_output_wgs84_lati.setText(str(deci_latitude))
            self.dialog.le_output_wgs84_long.setText(str(deci_longitude))

        rectangle = epsg_transformation.transform(QgsRectangle(
            deci_longitude - scale,
            deci_latitude - scale,
            deci_longitude + scale,
            deci_latitude + scale,
        ))

        point_epsg_project = QgsGeometry(
            QgsPoint(deci_longitude, deci_latitude))
        point_epsg_project.transform(epsg_transformation)
        canvas.setExtent(rectangle)
        canvas.refresh()

        point_xy = point_epsg_project.asPoint()

        self.dialog.le_output_lati.setText(str(round(point_xy[1], 6)))
        self.dialog.le_output_long.setText(str(round(point_xy[0], 6)))

    def run(self):
        if self.first_start:
            self.first_start = False
            self.dialog = LocatePluginDialog()
            self.dialog.bt_locate.clicked.connect(self.convert)

        self.clear()
        self.dialog.show()
