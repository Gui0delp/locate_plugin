from .locate_plugin import LocatePlugin

def classFactory(iface):
    return LocatePlugin(iface)
